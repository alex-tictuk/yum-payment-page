
const fs = require('fs');
const http = require('http');
const https = require('https');
const cors = require('cors')
const privateKey  = fs.readFileSync('sslcert/server.key', 'utf8');
const certificate = fs.readFileSync('sslcert/server.crt', 'utf8');
const credentials = {key: privateKey, cert: certificate};

var express = require("express");
var app = express();
var path = require("path");
const port = 3000;
var proxy = require("express-http-proxy");

app.use("/", express.static(path.join(__dirname)));
app.use("/script", express.static(path.join(__dirname, "../dist")));
// app.use(
//   "/proxy",
//   [
//     cors(),
//     proxy("https://commerce.dev.yumconnect.dev", {
//     proxyReqPathResolver: function (req) {
//       console.log("proxy url", req.url);
//       return req.url;
//     },
//   })]
// );

app.use(
  "/proxy",
  [
    cors(),
    proxy("http://localhost:3005", {
    proxyReqPathResolver: function (req) {
      console.log("proxy url", req.url);
      return req.url;
    },
  })]
);

var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

const ports = { http: 80, https: 443 }

httpServer.listen(ports.http);
httpsServer.listen(ports.https);

console.log('Listening on ', ports)