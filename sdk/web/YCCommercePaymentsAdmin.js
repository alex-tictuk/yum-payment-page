let processors = require("./processors/index.js");
const {
  applePayToken,
  giftCardToken,
  googlePayToken,
  savedPaymentToken,
} = require("./common/index");
class YCCommercePaymentsAdmin {
  /**
   * @param {Object} options - options to construct the YCCommercePaymentsAdmin sdk
   * @param {String} options.processor_id - processor id returned from createCardCaptureConfig !Yum platform GraphQL request
   * @param {String} options.processor_script_source - processor script source returned from GraphQL
   * @param {String} options.processor_script_config - processor script config returned from GraphQL
   * @param {Boolean?} options.debug - OPTIONAL, defaults to false. Enable debug logs on console
   */
  constructor(
    options = {
      processor_id: "",
      processor_script_source: "",
      processor_script_config: "",
      debug: false,
    }
  ) {
    const {
      processor_id,
      processor_script_source,
      processor_script_config,
      debug,
    } = options;

    this.processor_id = processor_id;
    this.processor_script_source = processor_script_source;
    this.debug = debug;

    try {
      this.processor_script_config = JSON.parse(
        Buffer.from(processor_script_config, "base64").toString("ascii")
      );
      this.customer_id = this.processor_script_config.customer_id;
      this.customer_type = this.processor_script_config.customer_type;
    } catch (ex) {
      console.error("could not parse processor script config");
      throw ex;
    }
  }

  /**
   * @param {Object} options
   * @param {String?} options.html_element_id - Optional, html element id of the div in the HTML document, where the iFrame will be loaded, defaults to "yc-payments-carcdcapture"
   * @param {String?} options.html_element - Optional, html element in the HTML document, where the iFrame will be loaded, defaults to "yc-payments-carcdcapture"
   * @param {Object} options.onSuccess - success callback. Payment token string will be passed to callback function.
   * @param {String} options.onError - error callback. Error object will be passed to callback function.
   */
  captureCard(
    options = {
      html_element_id: "yc-payments-carcdcapture",
      html_element: null,
      onError: (ex) => {
        return;
      },
      onSuccess: (paymentMethod) => {
        return;
      },
    }
  ) {
    let { html_element_id, html_element, onError, onSuccess } = options;
    try {
      if (html_element) {
        if (html_element.id) {
          html_element_id = html_element.id;
        } else {
          onError(
            new Error(
              "Invalid DOM element received, no element id found",
              html_element
            )
          );
          return;
        }
      }

      this.html_element_id = html_element_id;

      YCCommercePaymentsAdmin.checkIfDivExists(html_element_id);

      this.mountDiv({
        processor_data: {
          processor_id: this.processor_id,
          processor_script_config: this.processor_script_config,
          processor_script_src: this.processor_script_source,
        },
        onError,
        onSuccess,
      });
    } catch (ex) {
      console.error(ex);
      onError(ex);
    }
  }

  mountDiv({
    processor_data,
    onError = (ex) => {},
    onSuccess = (tender) => {},
  }) {
    try {
      let debug = this.debug;
      YCCommercePaymentsAdmin.checkIfDivExists(this.html_element_id);
      const div = document.getElementById(this.html_element_id);

      const loadProcessor = () => {
        let processor = processors.get_processor(processor_data.processor_id);

        if (typeof processor.onload !== "function") {
          throw new Error("Processor function is misconfigured");
        }

        processor.onload({
          processor_script_config: processor_data.processor_script_config,
          debug,
          onSuccess,
          onError,
        });
      };

      if (processor_data.processor_script_src) {
        if (processor_data.processor_script_config) {
          let script = document.createElement("script");
          script.src = processor_data.processor_script_src;
          script.onload = function () {
            loadProcessor();
          };

          div.appendChild(script);
        }
      } else {
        loadProcessor();
      }
    } catch (ex) {
      console.error(ex);
      onError(ex);
    }
  }

  static checkIfElementExists(element_id, type) {
    const element = document.getElementById(element_id);
    if (!element) {
      throw new Error(`No element found with id "${element_id}"`);
    }

    if (element.nodeName && element.nodeName.toLowerCase() !== type) {
      throw new Error(`No "${type}" element found with id "${element_id}"`);
    }

    return true;
  }

  static checkIfDivExists(element_id) {
    return this.checkIfElementExists(element_id, "div");
  }

  logDebug(...args) {
    if (this.debug) {
      console.log(...args);
    }
  }

  tokenizeApplePay(tokenData) {
    return applePayToken(this.customer_id, this.customer_type, tokenData);
  }

  tokenizeGooglePay(tokenData) {
    return googlePayToken(this.customer_id, this.customer_type, tokenData);
  }

  tokenizeGiftCard(giftCardData) {
   return giftCardToken(this.customer_id, this.customer_type, giftCardData);
  }

  tokenizeSavedPayment(savedPaymentData) {
    return savedPaymentToken(
      this.customer_id,
      this.customer_type,
      savedPaymentData
    );
  }
}

module.exports = YCCommercePaymentsAdmin;
