class FDUcomMock{
  constructor(config, debug){
    this.config = config
    this.tender = {}
    this.debug = debug || false
  }

  getTender(){
    return this.tender
  }

  fducommockSubmit(){
    this.resetButtons()
    this.setSubmitDisabled()
  }

  resetButtons(){
    var buttons = document.querySelectorAll('button')
    for (var i = 0; i < buttons.length; i++){
      buttons[i].style.color = 'black'
      buttons[i].style['background-color'] = 'white'
    }
  }

  toggleSelectedButton(type){
    this.resetButtons()
    var element = document.querySelector(`.${type}`)
    element.style.color = 'white'
    element.style['background-color'] =
      'rgb(var(--pure-material-primary-rgb, 33, 150, 243))'
  }

  setSubmitEnabled(){
    var fducommockSubmit = document.querySelector('.fducommockSubmit')
    fducommockSubmit.disabled = false
    fducommockSubmit.style.color = 'white'
    fducommockSubmit.style['background-color'] = 'lightgreen'
  }

  setSubmitDisabled(){
    var fducommockSubmit = document.querySelector('.fducommockSubmit')
    fducommockSubmit.disabled = true
    fducommockSubmit.style.color = 'darkgrey'
    fducommockSubmit.style['background-color'] = 'white'
  }

  setToken(type){
    this.toggleSelectedButton(type)
    this.setSubmitEnabled()
    if (type === 'bankcard'){
      this.tender = {
        tender_type: 'BC',
        tender_token: {
          token_value: 'b3959936-9288-4f61-a074-8bdd83d81fde',
          protection_source: 'UC',
          processor_source: 'FDM',
          token_type: 'CLAIM_CHECK_NONCE'
        },
        processor_session_token: 'VaYIf1CpHzNZH7YrzLGZ0DA1OlQt',
        processor_customer_id: this.config.fdCustomerId,
        tender_alias: 'Visa 1111',
        store: !!this.config.fdCustomerId,
        tender_product_type: 'VS',
        last_four: '1111'
      }
    }
    else if (type === 'googlepay'){
      this.tender = {
        tender_type: 'MP',
        tender_product_type: 'GP',
        tender_token: {
          version: 'ECv1',
          data:
            '{"encryptedMessage":"2W1NU18IP95H+MxusJIBRP0K1/YFIZDJGOBbrLLhaE4yqEjUsIFtsqtbD3+T6JuYXlrnwKazT6Sn9xY9yS8+Z0a4GoEq7j4/o36/mjsu282xEtOHRYVhkCw2MlkkDrfMWtY95DERQSRDxRYvwWL+okuhgjwkWniTrmw6qtN+xFF3I/P8pNfFpzT3Xn1zssxfjIyv6dcrXiZnZynbvhQoc2k49DnmIg5kb78zKusu8ZUCmgLFGDu8UYypV7yS6zY3GuKJpqcyQ1aARETnnhm1QTrkjPQeto+5wBMJuDLm5bSbzRNFm4se6fol8yJESPJUoenZZmI+ph9LoRs3vfOY5bufEgXLJ7o2QRzPcqvjZKHVQorIbukRzzWImOsSviN6WVvh0tqQwECBbKGu8jtwLA3NlXlfjOd9ZXCiQhZmzz6HSJetK8/TBamugdhcif0kHXtDnJK8ADMS9jrTFhhdBxe++rLFqaDzW+WevQZoVHMSCRXfpjT3OLqR5Zh6WK20S1apy2TmV4Dag1Tq/sO4UkDR7o9ghqZZPcFUCXMEbdnx75sWNr/1Toqiiq40kBJJ35G9cRTYiFVpTzo8x9E\\u003d","ephemeralPublicKey":"BIdn6hw9ljHEU21uoTPJQJ4HQYwSxkZWmYsxwWDaVolqg/fRH7IKoTcQL80vg9ME5GzzAx+jchb4QOXrSU4xQC8\\u003d","tag":"m/1+qZX4vfz9/6E0FgdwE4GqmEHL/cZup1MFUvlrr0g\\u003d"}',
          signature:
            'MEUCIBfn/fwjccfSt5zeYh+55Rg6daE/Bl/CQb+Lrfxs+aiQAiEAxan1DFoQAu5SOPmt53WONmIzC75CaZvMT0OcUOOwEDg='
        }
      }
    }
    else if (type === 'applepay'){
      this.tender = {
        tender_type: 'MP',
        tender_product_type: 'AP',
        tender_token: {
          version: 'EC_v1',
          data:
            'iQhe7jDoda+55PsN6Ub0S9LDHYPkdPd/OlUskoe5tAYw5GkppykO6f2q3/ckpmuap5/WJDFW4b+RlzRODW1wX38iNHRXTbP3GJPW7xgRoUazUQ+DAg5f4WQm51GjNGre8G9yQhxSunPTLbSnxoUQPSCL58hMOVdyw/cDsdXvC9WmWCn/SNsIhNQVWKyPDYUfQv20FC3Fr7x3AQiDKgjkek8zMWBfCjkLh5+foDq2VxkDl7fCVnvnCmGwc5AVi3roLTemd+83jkrSeBMN233gNPBL6ze0Zp7xNEMFe1PwfEuaqEeVDKfLeJ+arFvvp0G43iXjVtvU7RH+6lX95gQ+lOaX4LtJukS8qYQ9axSj2oozEag5Uey+qfhyU55NVsXb+jErwslE83t9DkR56LapFEJ1RJFyCOjPpfjDy51cH0GLSLGaWMxLN0g7fHlTaNLo/A2H65Mptfzr6WaJr472lhMFWFzwsbMkQUdrGA==',
          signature:
            'MIAGCSqGSIb3DQEHAqCAMIACAQExDzANBglghkgBZQMEAgEFADCABgkqhkiG9w0BBwEAAKCAMIIB0zCCAXkCAQEwCQYHKoZIzj0EATB2MQswCQYDVQQGEwJVUzELMAkGA1UECAwCTkoxFDASBgNVBAcMC0plcnNleSBDaXR5MRMwEQYDVQQKDApGaXJzdCBEYXRhMRIwEAYDVQQLDAlGaXJzdCBBUEkxGzAZBgNVBAMMEmQxZHZ0bDEwMDAuMWRjLmNvbTAeFw0xOTA3MjUxNjI3NDlaFw0zOTA3MjAxNjI3NDlaMHYxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJOSjEUMBIGA1UEBwwLSmVyc2V5IENpdHkxEzARBgNVBAoMCkZpcnN0IERhdGExEjAQBgNVBAsMCUZpcnN0IEFQSTEbMBkGA1UEAwwSZDFkdnRsMTAwMC4xZGMuY29tMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAErnHhPM18HFbOomJMUiLiPL7nrJuWvfPy0Gg3xsX3m8q0oWhTs1QcQDTT+TR3yh4sDRPqXnsTUwcvbrCOzdUEeTAJBgcqhkjOPQQBA0kAMEYCIQCXW2kjA36LdnEqc0qHb82FIYShdEk3hgPGxZDf6PfXygIhANrrpo/SY2YorozC73ZuQDtsGK3PHTdQZja5AgdEXy0yAAAxggFSMIIBTgIBATB7MHYxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJOSjEUMBIGA1UEBwwLSmVyc2V5IENpdHkxEzARBgNVBAoMCkZpcnN0IERhdGExEjAQBgNVBAsMCUZpcnN0IEFQSTEbMBkGA1UEAwwSZDFkdnRsMTAwMC4xZGMuY29tAgEBMA0GCWCGSAFlAwQCAQUAoGkwGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMjAwODI0MDU1ODI0WjAvBgkqhkiG9w0BCQQxIgQgeeN49Wj2lQiLw5gzKWqhMpXcjGUa2ilnOzvNrq80LhYwCgYIKoZIzj0EAwIERjBEAiBEAD0GZKUlSZsFdfRwVFipyUtwlfpsCdQe+yDDhYqtuwIgJseSLTl9/l83qAmxXXEQft8fe1Gj/aQEMb7dJRBVQSoAAAAAAAA=',
          decrypt_alias: 'merchant.com.applepay',
          application_data: 'VEVTVA==',
          header: {
            ephemeral_public_key:
              'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEcVpsjkzx3uU+mkA7V0gZeAZkeUNIBKrGwIxKMgoVCnvgSZDLDQYxZC7UFbeyg89eiEv1eZ2Ch38efO+xGvJhhg==',
            public_key_hash: 'RLP3SbLJR7bWmJFAfBa8hV5BQhRJ1AAhBrsjkNXKW7U=',
            transaction_id: '31323334353637'
          }
        }
      }
    }
  }
}

let div = `
    <div
      style="
        width: 50%;
        height: fit-content;
        margin: auto;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
      "
    >
      <button
        class="bankcard"
        onClick="fducommockFunctions.setToken('bankcard')"
        style="
          margin: 10px;
          position: relative;
          display: inline-block;
          box-sizing: border-box;
          border: none;
          border-radius: 4px;
          padding: 0 16px;
          min-width: 125px;
          height: 36px;
          vertical-align: middle;
          text-align: center;
          text-overflow: ellipsis;
          text-transform: uppercase;
          color: black;
          background-color: white;
          box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2),
            0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
          font-family: var(
            --pure-material-font,
            'Roboto',
            'Segoe UI',
            BlinkMacSystemFont,
            system-ui,
            -apple-system
          );
          font-size: 14px;
          font-weight: 500;
          line-height: 36px;
          overflow: hidden;
          outline: none;
          cursor: pointer;
          transition: box-shadow 0.2s;
        "
      >
        Bank Card
      </button>
      <button
        class="googlepay"
        onClick="fducommockFunctions.setToken('googlepay')"
        style="
          margin: 10px;
          position: relative;
          display: inline-block;
          box-sizing: border-box;
          border: none;
          border-radius: 4px;
          padding: 0 16px;
          min-width: 125px;
          height: 36px;
          vertical-align: middle;
          text-align: center;
          text-overflow: ellipsis;
          text-transform: uppercase;
          color: black;
          background-color: white;
          box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2),
            0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
          font-family: var(
            --pure-material-font,
            'Roboto',
            'Segoe UI',
            BlinkMacSystemFont,
            system-ui,
            -apple-system
          );
          font-size: 14px;
          font-weight: 500;
          line-height: 36px;
          overflow: hidden;
          outline: none;
          cursor: pointer;
          transition: box-shadow 0.2s;
        "
      >
        Google Pay
      </button>
      <button
        class="applepay"
        onClick="fducommockFunctions.setToken('applepay')"
        style="
          margin: 10px;
          position: relative;
          display: inline-block;
          box-sizing: border-box;
          border: none;
          border-radius: 4px;
          padding: 0 16px;
          min-width: 125px;
          height: 36px;
          vertical-align: middle;
          text-align: center;
          text-overflow: ellipsis;
          text-transform: uppercase;
          color: black;
          background-color: white;
          box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2),
            0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
          font-family: var(
            --pure-material-font,
            'Roboto',
            'Segoe UI',
            BlinkMacSystemFont,
            system-ui,
            -apple-system
          );
          font-size: 14px;
          font-weight: 500;
          line-height: 36px;
          overflow: hidden;
          outline: none;
          cursor: pointer;
          transition: box-shadow 0.2s;
        "
      >
        Apple Pay
      </button>
      <button
        class="fducommockSubmit"
        type="submit"
        onClick="fducommockFunctions.fducommockSubmit()"
        disabled="true"
        style="
          margin: 10px;
          position: relative;
          display: inline-block;
          box-sizing: border-box;
          border: none;
          border-radius: 4px;
          padding: 0 16px;
          min-width: 125px;
          height: 36px;
          vertical-align: middle;
          text-align: center;
          text-overflow: ellipsis;
          text-transform: uppercase;
          color: darkgrey;
          background-color: white;
          box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2),
            0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
          font-family: var(
            --pure-material-font,
            'Roboto',
            'Segoe UI',
            BlinkMacSystemFont,
            system-ui,
            -apple-system
          );
          font-size: 14px;
          font-weight: 500;
          line-height: 36px;
          overflow: hidden;
          outline: none;
          cursor: pointer;
          transition: box-shadow 0.2s;
        "
      >
        submit
      </button>
    </div>
`

let onload = ({ processor_script_config, debug, onSuccess = (tender) => {}, onError = (ex) => {} }) => {
  try {
    this.debug = debug
    let element = document.getElementById(processor_script_config.mountId)
    element.innerHTML = div

    fducommockFunctions = new FDUcomMock(processor_script_config,debug)

    document.querySelector('.fducommockSubmit').onclick = () => {
      fducommockFunctions.fducommockSubmit()
      onSuccess(fducommockFunctions.getTender())
    }
  }
  catch (ex){
    console.error(ex)
    onError(ex)
  }
}

module.exports = {
  onload
}
