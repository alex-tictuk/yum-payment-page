class FDuCom{
  static getTenderMapping(){
    return {
      CREDIT: 'BC',
      DEBIT: 'BC',
      PREPAID: 'GC'
    }
  }

  static getCardTypeMapping(){
    return {
      VISA: 'VS',
      MASTERCARD: 'MC',
      DISCOVER: 'DV',
      AMEX: 'AX',
      PREPAID: 'GC',
      VAULTED_ACCOUNT: 'VA',
      APPLE_PAY: 'AP',
      ANDRIOD_PAY: 'AD',
      PAYPAL: 'PA',
      TOKEN: 'TO',
      NONCE: 'NO',
      VCO: 'VC',
      VENMO: 'VE',
      AMAZON_PAY: 'AZ',
      VIPPS: 'VI',
      FLEET: 'FL'
    }
  }

  static getProtectionSource(){
    return {
      TRANS_ARMOR: 'TA',
      UCOM: 'UC',
      'No protection': 'NO'
    }
  }

  static formTender(config, response) {
    response = JSON.parse(response)

    if (!response || !response.token) {
      throw new Error('Invalid card details')
    }

    var tender_token = {
      token_value: response.token.tokenId,
      protection_source: this.getProtectionSource()[response.token.tokenProvider],
      processor_source: 'FD',
      token_type: response.token.tokenType
    }

    return {
      name: response.nickName,
      card_type: response.credit.cardType,
      last_four: response.credit.alias,
      payment_method: Buffer.from(
        JSON.stringify({
          customer_id: config.customer_id,
          customer_type: config.customer_type,
          token: {
            tender_type: this.getTenderMapping()[response.type],
            tender_token: tender_token,
            processor_session_token: config.accessToken,
            processor_customer_id: config.processor_customer_id,
            tender_alias:
              response.credit.cardType.charAt(0).toUpperCase() +
              response.credit.cardType.slice(1).toLowerCase() +
              " " +
              response.credit.alias,
            store:
              config.customer_type === "anonymous"
                ? false
                : response.isSaveCard,
            tender_product_type:
              this.getCardTypeMapping()[response.credit.cardType],
            last_four: response.credit.alias,
          },
        })
      ).toString("base64"),
    };
  }
}

let onload = ({ processor_script_config, debug, onSuccess = () => {}, onError = (ex) => {} }) => {
  try {
    ucomSDK.init(
      {
        ...processor_script_config,
        debug
      },
      (uComResponse) => {
        try {
          let tender = FDuCom.formTender(processor_script_config, uComResponse)
          onSuccess(tender)
          ucomSDK.stop()
        }
        catch (ex){
          console.error(ex)
          onError(ex)
        }
      }
    )
    ucomSDK.start()
  }
  catch (ex){
    console.error(ex)
    onError(ex)
  }
}

module.exports = {
  onload
}
