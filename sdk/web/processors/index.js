module.exports = {
  get_processor: function(processor_id){
    if (!processor_id){
      throw new Error('Invalid processor information received')
    }

    processor_id = processor_id.toLowerCase()
    let processor = processors[processor_id]

    if (!processor){
      throw new Error('Invalid processor information received')
    }

    return processor
  }
}

const processors = {
  fducom: require('./fducom'),
  fducommock: require('./fducommock')
}
