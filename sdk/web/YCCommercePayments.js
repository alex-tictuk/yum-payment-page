let processors = require('./processors/index.js')
const {applePayToken, giftCardToken, googlePayToken, savedPaymentToken} = require('./common/index')

class YCCommercePayments {
  /**
   * @param {Object} options - options to construct the YCCommercePayments sdk
   * @param {'test' | 'production'} options.environment - "test" or "production". Defines the commerce backend URL that will be used
   * @param {String} options.access_token - Authorization token for the sdk to communicate with the commerce back-end
   * @param {String} options.org_id - identifier for brand organization
   * @param {String?} options.locale_code - OPTIONAL, language for localization. Defaults to "en"
   * @param {Boolean?} options.debug - OPTIONAL, defaults to false. Enable debug logs on console
   */
  constructor(
    options = {
      environment: "test",
      access_token: "",
      debug: false,
      org_id: "yumstore_us",
      locale_code: "en",
    }
  ) {
    const { environment, access_token, debug, org_id, locale_code } = options;
    if (options.environment === "production") {
      this.base_url = `https://commerce.prod.yumconnect.dev/${org_id}/storefront/graphql`;
    } else {
      this.base_url = `https://commerce.staging.yumconnect.dev/${org_id}/storefront/graphql`;
    }

    if (!options.access_token) {
      throw new Error("Missing required constructor value: access_token");
    }

    try {
      const payload = JSON.parse(
        Buffer.from(
          access_token.replace(/bearer\s/gi, "").split(".")[1],
          "base64"
        ).toString("ascii")
      );
      this.customer_id = payload.sub;
      this.customer_type = payload.is_guest ? "anonymous" : "registered";
    } catch (ex) {
      console.error("could not parse bearer token");
      throw ex;
    }

    this.environment = environment;
    this.access_token = access_token;
    this.debug = debug;
    this.org_id = org_id;
    this.locale_code = locale_code;
  }

  /**
   * @param {Object} options
   * @param {String?} options.html_element_id - Optional, html element id of the div in the HTML document, where the iFrame will be loaded, defaults to "yc-payments-carcdcapture"
   * @param {String?} options.html_element - Optional, html element in the HTML document, where the iFrame will be loaded, defaults to "yc-payments-carcdcapture"
   * @param {Object} options.page_type - page type requested. GUEST_CHECKOUT, USER_CHECKOUT, or USER_SAVE_NEW_CARD
   * @param {String} options.payment_method_type - payment method type. Only "BANK_CARD" currently supported
   * @param {Object} options.onSuccess - success callback. Payment token string will be passed to callback function.
   * @param {String} options.onError - error callback. Error object will be passed to callback function.
   */
  captureCard(
    options = {
      html_element_id: "yc-payments-carcdcapture",
      html_element: null,
      page_type: "GUEST_CHECKOUT",
      payment_method_type: "BANK_CARD",
      onError: (ex) => {
        return;
      },
      onSuccess: (paymentMethod) => {
        return;
      },
    }
  ) {
    let {
      html_element_id,
      page_type,
      html_element,
      payment_method_type,
      onError,
      onSuccess,
    } = options;

    if (html_element) {
      if (html_element.id) {
        html_element_id = html_element.id;
      } else {
        onError(
          new Error(
            "Invalid DOM element received, no element id found",
            html_element
          )
        );
        return;
      }
    }

    this.html_element_id = html_element_id;

    YCCommercePayments.checkIfDivExists(html_element_id);

    this.getProcessorInfo({
      page_type,
      payment_method_type,
      access_token: this.access_token,
    }) //TODO: Change
      .then((res) => {
        this.logDebug("response", res);
        const {
          data: { beginCardCapture },
        } = res;
        if (beginCardCapture.__typename !== "BeginCardCaptureSuccess") {
          delete beginCardCapture.__typename;
          return onError(beginCardCapture);
        }
        beginCardCapture.processorScriptConfig = JSON.parse(
          Buffer.from(
            beginCardCapture.processorScriptConfig,
            "base64"
          ).toString("ascii")
        );
        this.mountDiv({
          processor_data: {
            processor_id: beginCardCapture.processorId,
            processor_script_config: beginCardCapture.processorScriptConfig,
            processor_script_src: beginCardCapture.processorScriptSource,
          },
          onError,
          onSuccess,
        });
      })
      .catch((ex) => {
        console.error(ex);
        onError(ex);
      });
  }

  mountDiv({
    processor_data,
    onError = (ex) => {},
    onSuccess = (tender) => {},
  }) {
    try {
      let debug = this.debug;
      YCCommercePayments.checkIfDivExists(this.html_element_id);
      const div = document.getElementById(this.html_element_id);

      const loadProcessor = () => {
        let processor = processors.get_processor(processor_data.processor_id);

        if (typeof processor.onload !== "function") {
          throw new Error("Processor function is misconfigured");
        }

        processor.onload({
          processor_script_config: processor_data.processor_script_config,
          debug,
          onSuccess,
          onError,
        });
      };

      if (processor_data.processor_script_src) {
        if (processor_data.processor_script_config) {
          let script = document.createElement("script");
          script.src = processor_data.processor_script_src;
          script.onload = function () {
            loadProcessor();
          };

          div.appendChild(script);
        }
      } else {
        loadProcessor();
      }
    } catch (ex) {
      console.error(ex);
      onError(ex);
    }
  }

  getProcessorInfo(options) {
    const { page_type, payment_method_type, access_token } = options;

    const payload = {
      query: `mutation createCardCaptureConfig  {
        beginCardCapture (input: {
          paymentMethodType: ${payment_method_type},
          pageType: ${page_type},
          localeCode: "${this.locale_code}",
        }) {
          __typename
          ... on BeginCardCaptureSuccess {
            processorId
            processorScriptSource
            processorScriptConfig
          }
          ... on PaymentCustomerCreateError {
            message
            reason
          }
          ... on PaymentProcessorError {
            message
            reason
          }
        }
      }`,
    };

    this.logDebug(this.base_url, access_token, payload);

    return fetch(this.base_url, {
      method: "POST",
      body: JSON.stringify(payload),
      headers: {
        "Content-Type": "application/json",
        Authorization: `${access_token}`,
      },
    }).then((res) => {
      if (!(res.status >= 200 && res.status < 300)) {
        throw new Error(`Error response from server ${res.status}`);
      }
      return res.json();
    });
  }

  static checkIfElementExists(element_id, type) {
    const element = document.getElementById(element_id);
    if (!element) {
      throw new Error(`No element found with id "${element_id}"`);
    }

    if (element.nodeName && element.nodeName.toLowerCase() !== type) {
      throw new Error(`No "${type}" element found with id "${element_id}"`);
    }

    return true;
  }

  static checkIfDivExists(element_id) {
    return this.checkIfElementExists(element_id, "div");
  }

  logDebug(...args) {
    if (this.debug) {
      console.log(...args);
    }
  }

  tokenizeApplePay(tokenData) {
    return applePayToken(this.customer_id, this.customer_type, tokenData);
  }

  tokenizeGooglePay(tokenData) {
    return googlePayToken(this.customer_id, this.customer_type, tokenData);
  }

  tokenizeGiftCard(giftCardData) {
    return giftCardToken(this.customer_id, this.customer_type, giftCardData);
  }

  tokenizeSavedPayment(savedPaymentData) {
    return savedPaymentToken(this.customer_id, this.customer_type, savedPaymentData);
  }
}

module.exports = YCCommercePayments
