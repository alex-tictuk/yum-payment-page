module.exports = (customerId, customerType, giftCardData) => {
  const {
      card_number,
      security_code
  } = giftCardData;
  return Buffer.from(JSON.stringify({
    customer_id: customerId,
    customer_type: customerType,
    token: {
      tender_type: "GC",
      tender_product_type: "GC",
      card_number,
      security_code,
      tender_token: {
        protection_source: "NO",
        processor_source: "FD", //TODO: remove necessity of sending this info
      },
    },
  })).toString("base64");
};
