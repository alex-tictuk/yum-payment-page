module.exports = (customerId, customerType, tokenData) => {
  const { version, data, signature, decrypt_alias, application_data, header: { ephemeral_public_key, public_key_hash, transaction_id}} = tokenData
  return Buffer.from(JSON.stringify({
    customer_id: customerId,
    customer_type: customerType,
    token: {
      tender_type: "MP",
      tender_product_type: "AP",
      tender_token: {
        version,
        data,
        signature,
        decrypt_alias,
        application_data,
        header: {
          ephemeral_public_key,
          public_key_hash,
          transaction_id,
        },
      },
    },
  })).toString("base64");
}