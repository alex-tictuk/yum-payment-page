module.exports = (customerId, customerType, tokenData) => {
  const {
    version,
    signed_message,
    signature
  } = tokenData;
  return Buffer.from(JSON.stringify({
    customer_id: customerId,
    customer_type: customerType,
    token: {
      tender_type: "MP",
      tender_product_type: "GP",
      tender_token: {
        version,
        data: signed_message,
        signature
      },
    },
  })).toString("base64");
};
