const productTypeMap = {
  VISA: "VS",
  MASTERCARD: "MC",
  DISCOVER: "DV",
  AMEX: "AX",
  PREPAID: "GC",
  VAULTED_ACCOUNT: "VA",
  APPLE_PAY: "AP",
  ANDRIOD_PAY: "AD",
  PAYPAL: "PA",
  TOKEN: "TO",
  NONCE: "NO",
  VCO: "VC",
  VENMO: "VE",
  AMAZON_PAY: "AZ",
  VIPPS: "VI",
  FLEET: "FL",
};

module.exports = (customerId, customerType, savedPayment) => {
  const {
    payment_method_id,
    default: is_default,
    name,
    card_type,
    last_four,
    requires_security_code_by,
    processor_tender_id,
    processor_type,
    security_code,
    processor_customer_id,
  } = savedPayment;
  return Buffer.from(JSON.stringify({
    customer_id: customerId,
    customer_type: customerType,
    token: {
      tender_type: "BC",
      tender_product_type: productTypeMap[card_type],
      tender_token: {
        token_value: "",
        protection_source: "UC",
        processor_source: processor_type,
        token_type: "CLAIM_CHECK_NONCE",
      },
      tender_alias: name,
      store: true,
      processor_type,
      processor_tender_id,
      processor_customer_id,
      tender_id: payment_method_id,
      requires_security_code_by,
      security_code,
      is_default,
      last_four,
    },
  })).toString("base64");
};
