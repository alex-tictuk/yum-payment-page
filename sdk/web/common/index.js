const tokenizeApplePay = require('./tokenizeApplePay')
const tokenizeGiftCard = require('./tokenizeGiftCard')
const tokenizeSavedPayment = require('./tokenizeSavedPayment')
const tokenizeGooglePay = require('./tokenizeGooglePay')

module.exports = {
  applePayToken: tokenizeApplePay,
  giftCardToken: tokenizeGiftCard,
  savedPaymentToken: tokenizeSavedPayment,
  googlePayToken: tokenizeGooglePay,
};