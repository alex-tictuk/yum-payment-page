# Processor Interfaces for the fornt-end sdk
## Steps:
1. Create Processor file in this format: `{{processor_id}}.js` (in lower case)
2. Create a function called `onload` which takes the following parameters: 
    ```js
        /**
         * 
         * @param {Object} options 
         * @param {Object} options.processor_script_config - Configuration object required to initialize the sdk
         * @param {Boolean?} options.debug - Enable debug logs on console
         * @param {Function} options.onSuccess - Callback function when payment is captured successfully - Passes the tender object as a parameter
         * @param {Function} options.onError - Callback function when an error occurs while capturing a payment - Passes the exception object as a parameter
         */
        onload({ processor_script_config, debug, onSuccess = () => {}, onError = (ex) =>    {} })
    ````
3. Make sure onSuccess and onError always passes the appropriate objects as a paremeter.