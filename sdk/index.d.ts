// Type definitions for payments-frontend-sdk
// Definitions by: Yum! Commerce Payments

export enum CardType {
  'AMEX',
  'VISA',
  'MASTERCARD',
  'DISCOVER',
  'DINERS_CLUB'
}

export enum PageType {
  "GUEST_CHECKOUT",
  "USER_CHECKOUT",
  "USER_SAVE_NEW_CARD",
}

export interface PaymentMethod {
  card_type: CardType;
  last_four: string;
  payment_method: string;
  name?: string;
}

export interface PaymentsOptions {
  environment: "test" | "production";
  access_token: string;
  org_id: string;
  debug?: boolean;
  locale_code?: string;
}

export interface PaymentsAdminOptions {
  processor_id: string;
  processor_script_source: string;
  processor_script_config: string;
  debug?: boolean;
}

export interface CaptureCardOptions {
  page_type: PageType;
  payment_method_type: "BANK_CARD"; // other types to be supported later
  onError: (err: Error) => any;
  onSuccess: (paymentMethod: PaymentMethod) => any;
  html_element_id?: string;
  html_element?: HTMLElement;
}

export interface CaptureCardOptionsAdmin {
  onError: (err: Error) => any;
  onSuccess: (paymentMethod: PaymentMethod) => any;
  html_element_id?: string;
  html_element?: HTMLElement;
}

export interface ApplePayToken {
  version: string;
  data: string;
  signature: string;
  decrypt_alias: string;
  application_data: string;
  header: {
    ephemeral_public_key: string;
    public_key_hash: string;
    transaction_id: string;
  };
}

export interface GooglePayToken {
  version: string;
  signed_message: string;
  signature: string;
}

export interface GiftCard {
  card_number: string;
  security_code?: string;
}

export interface SavedPayment {
  payment_method_id: string;
  card_type: CardType;
  last_four: string;
  processor_type: string;
  processor_tender_id: string;
  processor_customer_id: string;
  default?: boolean;
  name?: string;
  requires_security_code_by?: string;
  security_code?: string;
}

/**
 * SDK initialization for storefront (client-side) integrations using Yum! Commerce Auth Tokens
 */
export class YCCommercePayments {
  /**
   * Initializes the sdk
   */
  constructor(options: PaymentsOptions);

  /**
   * Renders the payment card entry from against a DOM element. Will call onSuccess callback with the paymentMethod created from the form entry.
   * onError will be called with the relevant error for any exceptions during execution.
   */
  captureCard(options: CaptureCardOptions): void;

  /**
   * Will format an Apple Pay payment method into the expected payment method format for Yum! Commerce Payments authorization requests
   */
  tokenizeApplePay(tokenData: ApplePayToken): string;

  /**
   * Will format an Google Pay payment method into the expected payment method format for Yum! Commerce Payments authorization requests
   */
  tokenizeGooglePay(tokenData: GooglePayToken): string;

  /**
   * Will format an gift card number and pin into the expected payment method format for Yum! Commerce Payments authorization requests
   */
  tokenizeGiftCard(giftCardData: GiftCard): string;

  /**
   * Will format saved payment data retrieved from the !Yum Commerce Payments API into the expected payment method format for Yum! Commerce Payments authorization requests
   */
  tokenizeSavedPayment(savedPaymentData: SavedPayment): string;
}

/**
 * SDK initialization for business integrations using pre-fetched configuration (server-side) using machine-to-machine tokens.
 */

export class YCCommercePaymentsAdmin {
  /**
   * Initializes the sdk
   */
  constructor(options: PaymentsAdminOptions);

  /**
   * Renders the payment card entry from against a DOM element. Will call onSuccess callback with the paymentMethod created from the form entry.
   * onError will be called with the relevant error for any exceptions during execution.
   */
  captureCard(options: CaptureCardOptionsAdmin): void;

  /**
   * Will format an Apple Pay payment method into the expected payment method format for Yum! Commerce Payments authorization requests
   */
  tokenizeApplePay(tokenData: ApplePayToken): string;

  /**
   * Will format an Google Pay payment method into the expected payment method format for Yum! Commerce Payments authorization requests
   */
  tokenizeGooglePay(tokenData: GooglePayToken): string;

  /**
   * Will format an gift card number and pin into the expected payment method format for Yum! Commerce Payments authorization requests
   */
  tokenizeGiftCard(giftCardData: GiftCard): string;

  /**
   * Will format saved payment data retrieved from the !Yum Commerce Payments API into the expected payment method format for Yum! Commerce Payments authorization requests
   */
  tokenizeSavedPayment(savedPaymentData: SavedPayment): string;
}