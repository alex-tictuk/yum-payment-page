const { expect } = require("chai");

describe("YCCommercePayments", async () => {
  it("should create a new object without passing a constructor", async () => {});
  it("should create a new object with the constructor", async () => {});
  it("should error out when used an invalid constructor param", async () => {});

  it("should fetch HTML using the cardcapture endpoint", async () => {});
  it("should throw an error if there's an error from the server", async () => {});
});
