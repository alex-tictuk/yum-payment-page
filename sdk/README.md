
# payments-frontend-sdk

## Quick Start Guide
The Yum! payments services require tokenized payment methods on payment authorization requests. This SDK provides methods for tokenization as well as a card entry form that securely captures bank card details.

For loading the card entry form, there are two options available for brand partners. For partners utilizing Yum! Customer authentication, the SDK will fetch and create the necessary configuration based on the values provided in the configuration context. For partners utilizing their own customer data and authenticating at an administrator level, the SDK is able to build and consume configuration independently fetched from the Yum! platform GraphQL.

#### Installation
```bash
npm i --save @yc-commerce/payments-frontend-sdk
```
#### Standard Initialization

<ins>Input Parameters:</ins>
`environment`: "test" or "production". Test utilizes the Yum! Platform staging environment.
`access_token`: a bearer json web token. For standard initialization, this is the logged in user or guest user's Yum! Platform authorization token.
`org_id`: string value for brand organization identifier.
`locale_code`: location for language localization. Only “en” currently supported.
`debug`: boolean flag to enable/disable debug logging

<ins>Available Organizations Identifiers:</ins>
Yum! Store: `yumstore_us`
KFC US: `kfc_us`
Taco Bell US: `tb_us`
Tictuk: `hb_us`
Habit Burger: `hb_us`
```js
const { YCCommercePayments } = require("@yc-commerce/payments-frontend-sdk");

let sdk = new YCCommercePayments({
  environment: "test",
  access_token: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
  org_id: "kfc_us",
  locale_code: "en",
  debug: true
});
```

#### Administrator Initialization
Prerequisite: configuration object must be requested from the Yum! platform GraphQL API. 

##### GraphQL API Request: createCardCaptureConfig
This request provides the configuration object that will be used by the administrator SDK initialization.

<ins>URL:</ins> https://commerce.dev.yumconnect.dev/{org_id_here}/business/graphql

<ins>Input Parameters:</ins>
`localeCode`: location for language localization. Only “en” currently supported.
`pageType`: the page type being requested
1.  `GUEST_CHECKOUT`: anonymous user guest checkout. No card saving.
2.  `USER_CHECKOUT`: registered user checkout. Optional card saving.  
3.  `USER_SAVE_NEW_CARD`: registered user card save.

`paymentMethodType`: payment entry type. Only “BANK_CARD” currently supported.
`userId`: user identification. Should be unique to an individual.

<ins>Body:</ins>
```graphql
mutation {
	createCardCaptureConfig(input: {
		localeCode:"en"
		pageType:GUEST_CHECKOUT
		paymentMethodType:BANK_CARD
		userId:"6388adb5-6cb9-44cf-bc80-fb40d5ef1271"
	}) {
		__typename
		... on BeginCardCaptureSuccess {
			processorId
			processorScriptSource
			processorScriptConfig
		}
		... on PaymentCustomerCreateError {
			reason
			message
		}
		... on PaymentProcessorError {
			reason
			message
		}
	}
}
```
<ins>Response:</ins>
```json
{
	"data": {
		"createCardCaptureConfig": {
			"__typename": "BeginCardCaptureSuccess",
			"processorId": "FDuCom",
			"processorScriptSource": "https://api-int.payeezy.com/ucom/v1/static/v2/js/ucom-sdk.js",
			"processorScriptConfig": "eyJyZWRpcmVjdFVybCI6Ind3dy5mb28uY29tIiwiYWNjZXNzVG9rZW4iOiJ6V3dkQ2t3VXBkbUFWajRkaHNHYlptejRnaWk3IiwiYXBpS2V5IjoiQ2FYUXppZVhvVUJGdlp5b2RtNkZXRG5iNk95R1I3Tm8iLCJmZEN1c3RvbWVySWQiOiI4MDg2NDlmZDUxOTI0ZDQzOTI1ZDUzNTEzODk2NTViOCIsInBhZ2VVcmwiOiJodHRwczovL2FwaS1pbnQucGF5ZWV6eS5jb20vdWNvbS92MS9ob3N0ZWQtcGFnZXMvcGFnZXMvMDk3MjY0ZGEtZTg1My00OWFiLTk1NzktZDNiMTAzOTgxYzNmIiwibW91bnRJZCI6InljLXBheW1lbnRzLWNhcmNkY2FwdHVyZSIsImVuY3J5cHRpb25LZXkiOiJNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQW5RbXZhR3VNQTlNd0x5MkRRRjZnNnJDT1VFcTQvbzE2MmFJTUFpRU1lTjlUUE1mNmNWNmZxQW5razVSdmJZbjhXMWNUdXEyazJzOFl3ZWlZYklXQmUyaHpITHlhK3J0WUFQUHMzdVo0VEVkd0NGU1QrbVMyNERCYUtIOU5Hc2htOGxLQ1dSWkZFcnl1YW1YeldVbEpXWVJzQ2ZNSkpoMWpzMWVsRlBZbmNGeUl5TkxuOTNaV2J3bzAvcTBxaWJvQWU4d3llOWNWbUFUczJIemxORnEycE9xOHR4cVVZWVV5MDNwZTF0Z3RDMzl6QUlZdjNqSklGZzExczc3YndvRkozK29lNHdIVmdraHlDTkNYSW80eUliT3pySjhBSmdnUXF5cjE4dnpKaGtjbHM2d0NjMGNtdlJNa3FaSWIrdUNrSitLNGJZUGI4aWRwZ3FVcXRyMmxUd0lEQVFBQiIsInByb2Nlc3Nvcl9jdXN0b21lcl9pZCI6IjgwODY0OWZkNTE5MjRkNDM5MjVkNTM1MTM4OTY1NWI4IiwiY3VzdG9tZXJfaWQiOiI2Mzg4YWRiNS02Y2I5LTQ0Y2YtYmM4MC1mYjQwZDVlZjEyNzEiLCJjdXN0b21lcl90eXBlIjoicmVnaXN0ZXJlZCJ9"
		}
	}
}
```

##### SDK Init

<ins>Input Parameters:</ins>
`processor_id`: payment processor identifier returned from the GraphQL API request.
`processor_script_source`: source location of payment processor hosted page data returned from GraphQL.
`processor_script_config`: configuration string returned from GraphQL that supplies the payment processor with the necessary configuration.
`debug`: boolean flag to enable/disable debug logging
```js
const { YCCommercePaymentsAdmin } = require("@yc-commerce/payments-frontend-sdk");

let sdk = new YCCommercePaymentsAdmin({
  processor_id: "FDuCom",
  processor_script_source: "https://api-int.payeezy.com/ucom/v1/static/v2/js/ucom-sdk.js",
  processor_script_config: "U29tZSBCQVNFNjQgU3RyaW5nIENvbmZpZ3VyYXRpb24gRGF0YQ==",
  debug: true
})
```
#### Entry Form Load For Card Tokenization

`captureCard` function renders the card entry form against a provided html id and will execute the onSuccess or onError callback functions on form submission.

<ins>Input Parameters:</ins>
`page_type`: the page type being requested:
1. `GUEST_CHECKOUT`: anonymous user guest checkout. No card saving.
2.  `USER_CHECKOUT`: registered user checkout. Optional card saving.  
3.  `USER_SAVE_NEW_CARD`: registered user card save.

`payment_method_type`: payment entry type. Only “BANK_CARD” currently supported.

`html_element_id`is the DOM element id (div recommended) where the iFrame would be rendered. Defaults to `yc-payments-carcdcapture`

The `onSuccess(paymentMethod)` This callback passes the the paymentMethod object as a parameter after successful entry and tokenization of bank card details.

`onError(err)` if there are any errors in the card capture process, this callback function can be used to proceed with the error flow in the page, for eg. Showing the user a user friendly error in the UI.

```js
sdk.captureCard({
	html_element_id:  "yc-payments-carcdcapture", //OPTIONAL, defaults to yc-payments-carcdcapture
	page_type: "GUEST_CHECKOUT", //Not required for administrator flow
	payment_method_type: "BANK_CARD", //Not required for administrator flow
	onSuccess: (paymentMethod) => {
		console.log("commerce paymentMethod string", paymentMethod);
	},
	onError: (err) =>  console.error("Something went wrong.", err)
});

```
#### Saved Payment Tokenization
Saved payment methods returned from the Yum! Platform GraphQL API can be tokenized for authorization with the below method.
```js
let paymentMethod = sdk.tokenizeSavedPayment({
	payment_method_id: "4cd1d74e-76e6-4668-8a8b-fa6d523a50d4",
	default: false,
	name: "Visa 1111",
	card_type: "VISA",
	last_four: "1111",
	requires_security_code_by: "2021-08-04T06:58:53.364Z",
	processorType: "FD"
})
```

#### Mobile Payment Tokenization
Payment tokens returned from mobile payment providers can be tokenized for authorization with the below methods.
##### Apple Pay
```js
let paymentMethod = sdk.tokenizeApplePay({
	version: "EC_v1",
	data: "kdHd..GQ==",
	signature: "MIAGCSqGSIb3DQEH...AAA",
  decrypt_alias: "merchant.com.applepay",
  application_data: "94ee0..C2",
	header: {
		transaction_id: "d3b28af..f8",
		ephemeral_public_key: "MFkwE..Q==",
		public_key_hash: "dxCK..6o="
	}
})
```

##### Google Pay
```js
let paymentMethod = sdk.tokenizeGooglePay({
	version: "ECv1",
	signed_message: "{\"encryptedMessage\":\"yprFUxsRpXF..e3KA\\u003d\",\"ephemeralPublicKey\":\"BBvcGK5..FeY\\u003d\",\"tag\":\"jBf/I0g..4\\u003d\"}",
	signature: "MEQCIH..U2K2TYQ=="
})
```

#### Gift Card Tokenization
Gift cards can be tokenized for authorization with the below method
```js
let paymentMethod = sdk.tokenizeGiftCard({
	card_number: "4111111111111111",
	security_code: "1234"
})
```

## Motivation

Card capture pages are hosted by the e-commerce backend. They have different domains/origins than the brands.

ex:
- Commerce Platform URL: https://commerce.dev.yumconnect.dev/
- Brand URL: https://www.kfc.com

When trying to load the existing card capture page using an iFrame inside https://www.kfc.com,

ex:
```html
<iframe
id="yc-payments-carcdcapture"
src="https://commerce.dev.yumconnect.dev/link/to/cardcapture/page"
></iframe>
```

We'd run into problems like this:
```
Refused to frame 'https://processor.host.com/' because an ancestor violates the following Content Security Policy directive: "frame-ancestors https://commerce.dev.yumconnect.dev".
```

The `payments-frontend-sdk` not only solves this issue by mounting the card capture directly on the brand page, but also provides an interface to render and capture card information without dealing with processor specific logic.


For use in browser environments it can be imported using a bundler such as [webpack](https://webpack.js.org/):

```html
<!DOCTYPE  html>
<html>
<body>
<div  id="yc-payments-carcdcapture"></div>
<script  src="./node_modules/@yc-commerce/payments-frontend-sdk/dist/bundle.js"></script>
<script>
let sdk = new payments_frontend_sdk.YCCommercePayments({
	environment: "test",
	access_token: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
	org_id: "kfc_us",
	locale_code: "en",
	debug: true
});

sdk.captureCard({
	html_element_id:  "yc-payments-carcdcapture", //OPTIONAL, defaults to yc-payments-carcdcapture
	page_type: "GUEST_CHECKOUT", //Not required for administrator flow
	payment_method_type: "BANK_CARD", //Not required for administrator flow
	onSuccess: (paymentMethod) => {
		console.log("commerce paymentMethod string", paymentMethod);
	},
	onError: (err) =>  console.error("Something went wrong.", err)
});
</script>
</body>
</html>

```

## TODOs

  

- [ ] Mock payments iFrame in SDK

- [ ] Backend and front-end versions

- [ ] Work on redirect urls with brands

- [ ] Better testing playground

- [ ] Native Mobile Wrappers (Kotlin, Swift, etc.)